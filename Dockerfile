FROM php:7.0-apache
COPY html_root/ /var/www/html/
RUN chown -R www-data:www-data /var/www/html/reports
