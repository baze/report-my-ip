# Introduction
This application is used to track changing IP addresses of your Raspberry PI's.
This project consists of [Server](https://gitlab.com/baze/report-my-ip) and [Client](https://gitlab.com/baze/report-my-ip-client) application.

**Server application** is installed on your central server and **Client application** on all your Raspberry PIs.

# 1) Start server
```
docker run -d -p 5111:80 registry.gitlab.com/baze/report-my-ip
```
Change the port (5111) if you want to use another port.

# 2) Start client in each Raspberry PI
```
    curl -sSL https://gitlab.com/baze/report-my-ip-client/raw/master/setup.sh | sh
```

**Client apps** setup will ask **servers ip:port** so client can send its IP there.
