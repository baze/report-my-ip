<?php

$datadir="reports";

// IF ip and name given, then write them to file
if ( isset($_GET["ip"]) && isset($_GET["name"]) )
{
	$ip=$_GET["ip"];
	$name=$_GET["name"];
	$time = time();

	$filename = md5("$name");

	file_put_contents("${datadir}/${filename}", "$time;$ip;$name" );
	echo "ok";
} else {
	echo "<h1>Clients</h1>";
	// IP and name not given, so we show reports
	$files = scandir("./$datadir");
	foreach( $files as $file){
		if ( $file == "." || $file == "..") continue;

		$words = explode( ";", file_get_contents("${datadir}/${file}") );

		if ( sizeof($words) == 3){
			$delta = time() - $words[0];
			echo "[".$words[2]."] ";
			echo "<a href=\"ssh://".$words[1]."\">";
			echo "[".$words[1]."]";
			echo "</a> ";
			echo round($delta/60,0). " min ago. <br />";
		}
	}
}

?>
